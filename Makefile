#
# Copyright (C) 2011, Digium, Inc.
# Paul Belanger <pabelanger@digium.com>
#
# This program is free software, distributed under the terms of
# the GNU General Public License Version 2.
#

all:

clean: _clean

_clean:

dist-clean: distclean

distclean: _clean
	rm -rf doc/api

install:

uninstall:

update:

asttest:

progdocs:
	(cat contrib/testsuite-doxygen) | doxygen -
